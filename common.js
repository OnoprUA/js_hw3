let userNumber = prompt("Please, enter an integer number");

if (isNaN(userNumber) || userNumber === null || userNumber === "") {
    alert("Sorry, no numbers");
} else {

    while (userNumber % 1 !== 0) {
        userNumber = prompt("Not an integer number. Please enter correct number");
    }
}

for (let numb = 1; numb <= userNumber; numb++) {

    if (numb % 5 === 0) {
        console.log(numb);
    }
}
